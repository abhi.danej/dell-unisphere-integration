import time
from bin.Datetime import myPrint, getCurrentDT
from bin.commons import readProperties, getNewEventIdList, checkLocalFile, getOldEventIdList, getLastProcessedEventTime
from bin.UnisphereHelper import getEventFromUnisphere

propJson = readProperties('conf/properties.props')

myPrint("START PROGRAM")

flag = True
counter = 1

while flag:
    myPrint("============================================")
    counter = counter + 1
    max_time = getLastProcessedEventTime(propJson)

    # get eventId list from dell
    newEventIdSet = getNewEventIdList(propJson)
    myPrint(f'Received total {len(newEventIdSet)} new events {newEventIdSet}')

    # if local file is present, compare with new_eventIdList and get only the newly_generated events
    if(False):
    # if(checkLocalFile(propJson)):
        oldEventIdSet = getOldEventIdList(propJson)
        myPrint(f'Stored events count, {len(oldEventIdSet)}')
        # print(oldEventIdSet)
        freshEventIdSet = newEventIdSet.difference(oldEventIdSet)
        myPrint(f'Extra events found: {len(freshEventIdSet)}, {freshEventIdSet}')
        for e in freshEventIdSet:
            myPrint(e)
            # get event E details from dell, 
                # if it is critical/fatal and its create_Time is greater than max_time ..then process it,
                # if processed store its create_time in set to calc new max_time

        # overrite local-eventid-list file with newEventIdSet
    else:
        myPrint("No local file")
        for eId in newEventIdSet:
            # get event E details from dell, 
                # if it is critical/fatal and its create_Time is greater than max_time ..then process it,
                # if processed store its create_time in set to calc new max_time
            eventJson = getEventFromUnisphere(eId)
            myPrint(eventJson)

        # overrite local-eventid-list file with newEventIdSet

    # store new eventIdList
    myPrint("Waiting for 5 seconds")
    time.sleep(5)

myPrint("END PROGRAM" )