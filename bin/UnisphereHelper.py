import xml.etree.ElementTree as ET

def getEventFromUnisphere(eventId):
    # pass eventId to REST API
    print(__name__)

    with open ('C:/Users/ADanej/Documents/My Stuff/workspace_nodejs/python/dell_unisphere/bin/Alert.txt','r') as f:
        f.readline()
        f.readline()
        alert = ''.join(f.readlines())
        alertStr = alert.replace('ns12:','')
        # print("XML ALERT", alertStr)
        f.close()

    root = ET.fromstring(alertStr)

    # tree: ET.parse('c:/Users/ADanej/Documents/My Stuff/workspace_nodejs/python/dell_unisphere/Alert.txt')
    # root: tree.getroot()  

    # print('=============')
    # for child in root:
    # print(child.tag, child.attrib)

    print("ROOT TAG", root.tag)
    alertJson = {
        "alertId": root.find('alertId').text,
        "state": root.find('state').text,
        "severity": root.find('severity').text,
        "type": root.find('type').text,
        "array": root.find('array').text,
        "object": root.find('object').text,
        "object_type": root.find('object_type').text,
        "created_date": root.find('created_date').text,
        "created_date_milliseconds": root.find('created_date_milliseconds').text,
        "description": root.find('description').text.replace('\n',''),
        "acknowledged": root.find('acknowledged').text
    }
    return alertJson