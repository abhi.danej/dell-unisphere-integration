from Datetime import MyDateTime, myPrint        # CASE 1 works for same directory 
# import Datetime                         # CASE 2, import file name without extension

# CASE 1, use direction classname or methodnames
year = MyDateTime(1579009073572).getDateTime().year
myPrint("")                                          

# CASE 2 use filename.classname() or filename.methodname()
# year = Datetime.MyDateTime(1579009073572).getDateTime().year
# Datetime.myPrint("")                      


print("Year", year)