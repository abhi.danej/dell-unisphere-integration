from .Datetime import myPrint
import xml.etree.ElementTree as ET
import json
import os.path


def getAlertObject(alertStr):
    root = ET.fromstring(alertStr)

    # tree: ET.parse('c:/Users/ADanej/Documents/My Stuff/workspace_nodejs/python/dell_unisphere/Alert.txt')
    # root: tree.getroot()  

    # print('=============')
    # for child in root:
    # print(child.tag, child.attrib)

    print("ROOT TAG", root.tag)
    alertJson = {
        "alertId": root.find('alertId').text,
        "state": root.find('state').text,
        "severity": root.find('severity').text,
        "type": root.find('type').text,
        "array": root.find('array').text,
        "object": root.find('object').text,
        "object_type": root.find('object_type').text,
        "created_date": root.find('created_date').text,
        "created_date_milliseconds": root.find('created_date_milliseconds').text,
        "description": root.find('description').text.replace('\n',''),
        "acknowledged": root.find('acknowledged').text
    }
    return alertJson

def readProperties(props_file):
    with open(props_file) as json_file:
        propJson = json.load(json_file)
        print(propJson['url'])
        json_file.close()
    return propJson

def getNewEventIdList(propJson):
    return set(['abc','def'])

def checkLocalFile(propJson):
    if os.path.isfile(propJson['dat_file']):
        return True
    else:
        return False

def getOldEventIdList(propJson):
    with open(propJson['dat_file'], 'r') as f:
        firstLine = f.readline()
        f.close()
    return (set(firstLine.split(' ')))

def getLastProcessedEventTime(propJson):
    firstLine = 0
    try:
        with open(propJson['time_file'], 'r') as f:
            firstLine = f.readline()
            f.close()
    except FileNotFoundError:
        myPrint(f'File not found {propJson["time_file"]}, setting last_event_processed_time as 0')
    else:
        myPrint(f'Last event was processed at {firstLine}')
    finally:
        return firstLine