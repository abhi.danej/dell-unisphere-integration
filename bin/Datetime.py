from datetime import datetime

class MyDateTime():
    def __init__(self, milliseconds):
        self.mills = milliseconds

    def getDateTime(self):
        dt1 = datetime.fromtimestamp(self.mills/1000)   # 1579009073572
        # print(dt1)
        # dt2 = datetime.strptime('01-14-2020 19:07:53.572','%m-%d-%Y %H:%M:%S.%f')
        # print("Date 2", dt2, "\nDate 2 Month ", dt2.month)
        return dt1

def myPrint(str):
    print(getCurrentDT(), str)

def getCurrentDT():
    return datetime.now()

# year = MyDateTime(1579009073572).getDateTime().year
# print("Year", year)